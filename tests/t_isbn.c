/*-
 * Copyright (c) 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "t_isbn_defs.h"

#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "isbn_local.h"

static const struct testcase {
	const char *isbn10, *isbn13;
} testcases[] = {
	{ "0-14-018802-9", "978-0-14-018802-8" },
	{ "0-14-118562-7", "978-0-14-118562-0" },
	{ "0-15-202398-4", "978-0-15-202398-0" },
	{ "0-15-204804-9", "978-0-15-204804-4" },
	{ "0-15-246503-0", "978-0-15-246503-2" },
	{ "0-15-601219-7", "978-0-15-601219-5" },
	{ "0-15-601386-X", "978-0-15-601386-4" },
	{ "0-15-601398-3", "978-0-15-601398-7" },
	{ "0-320-03916-1", "978-0-320-03916-4" },
	{ "0-320-08606-2", "978-0-320-08606-9" },
	{ "0-395-24005-0", "978-0-395-24005-2" },
	{ "0-464-24575-3", "978-0-464-24575-9" },
	{ "0-544-65649-0", "978-0-544-65649-9" },
	{ "0-544-69958-0", "978-0-544-69958-8" },
	{ "0-544-69959-9", "978-0-544-69959-5" },
	{ "0-544-70901-2", "978-0-544-70901-0" },
	{ "0-544-70902-0", "978-0-544-70902-7" },
	{ "0-544-79255-6", "978-0-544-79255-5" },
	{ "0-686-56567-3", "978-0-686-56567-3" },
	{ "0-7567-5189-6", "978-0-7567-5189-0" },
	{ "0-7859-2322-5", "978-0-7859-2322-0" },
	{ "0-7893-3866-1", "978-0-7893-3866-2" },
	{ "0-89190-331-3", "978-0-89190-331-4" },
	{ "0-9567215-9-1", "978-0-9567215-9-4" },
	{ "0-9571387-4-1", "978-0-9571387-4-2" },
	{ "1-291-68303-8", "978-1-291-68303-5" },
	{ "1-291-71893-1", "978-1-291-71893-5" },
	{ "1-328-47975-7", "978-1-328-47975-4" },
	{ "1-5071-5903-X", "978-1-5071-5903-3" },
	{ "1-5087-6312-7", "978-1-5087-6312-3" },
	{ "1-5148-9947-7", "978-1-5148-9947-2" },
	{ "1-5190-4703-7", "978-1-5190-4703-8" },
	{ "1-5229-6812-1", "978-1-5229-6812-2" },
	{ "1-55394-066-0", "978-1-55394-066-1" },
	{ "1-60796-415-5", "978-1-60796-415-5" },
	{ "1-64574-007-2", "978-1-64574-007-0" },
	{ "1-64574-008-0", "978-1-64574-008-7" },
	{ "1-64574-012-9", "978-1-64574-012-4" },
	{ "1-64606-875-0", "978-1-64606-875-3" },
	{ "1-64826-730-0", "978-1-64826-730-7" },
	{ "1-6948-9695-1", "978-1-6948-9695-7" },
	{ "1-77323-120-0", "978-1-77323-120-4" },
	{ "1-78487-417-5", "978-1-78487-417-9" },
	{ "1-84022-760-5", "978-1-84022-760-4" },
	{ "1-84749-824-8", "978-1-84749-824-3" },
	{ "1-85326-158-0", "978-1-85326-158-9" },
	{ "1-86205-066-X", "978-1-86205-066-2" },
	{ "1-907360-01-8", "978-1-907360-01-5" },
	{ "1-909621-55-2", "978-1-909621-55-8" },
	{ "1-912714-54-X", "978-1-912714-54-4" },
	{ "1-9790-2213-5", "978-1-9790-2213-2" },
	{ "1-9814-2382-6", "978-1-9814-2382-8" },
	{ "1-987817-94-X", "978-1-987817-94-2" },
	{ "2-07-045176-3", "978-2-07-045176-0" },
	{ "2-07-055551-8", "978-2-07-055551-2" },
	{ "2-07-061275-9", "978-2-07-061275-8" },
	{ "2-07-066722-7", "978-2-07-066722-2" },
	{ "2-07-066793-6", "978-2-07-066793-2" },
	{ "2-215-12359-1", "978-2-215-12359-0" },
	{ "2-215-12373-7", "978-2-215-12373-6" },
	{ "2-215-12616-7", "978-2-215-12616-4" },
	{ "2-215-12624-8", "978-2-215-12624-9" },
	{ "2-215-12841-0", "978-2-215-12841-0" },
	{ "2-215-13007-5", "978-2-215-13007-9" },
	{ "2-215-13324-4", "978-2-215-13324-7" },
	{ "2-7593-0882-0", "978-2-7593-0882-8" },
	{ "3-458-19411-8", "978-3-458-19411-8" },
	{ "3-596-52042-8", "978-3-596-52042-8" },
	{ "3-7306-0228-4", "978-3-7306-0228-7" },
	{ "3-7306-0229-2", "978-3-7306-0229-4" },
	{ "3-7481-3067-8", "978-3-7481-3067-3" },
	{ "3-86820-351-6", "978-3-86820-351-6" },
	{ "4-00-110916-6", "978-4-00-110916-0" },
	{ "4-00-110917-4", "978-4-00-110917-7" },
	{ "4-00-110918-2", "978-4-00-110918-4" },
	{ "4-00-110919-0", "978-4-00-110919-1" },
	{ "4-00-114001-2", "978-4-00-114001-9" },
	{ "4-00-115561-3", "978-4-00-115561-7" },
	{ "4-00-115676-8", "978-4-00-115676-8" },
	{ "4-00-220006-X", "978-4-00-220006-4" },
	{ "4-04-103656-9", "978-4-04-103656-3" },
	{ "4-04-298219-0", "978-4-04-298219-7" },
	{ "4-04-631163-0", "978-4-04-631163-4" },
	{ "4-06-148749-3", "978-4-06-148749-9" },
	{ "4-08-760494-2", "978-4-08-760494-8" },
	{ "4-10-212204-4", "978-4-10-212204-4" },
	{ "4-12-003643-X", "978-4-12-003643-9" },
	{ "4-16-791288-0", "978-4-16-791288-8" },
	{ "4-19-864019-X", "978-4-19-864019-4" },
	{ "4-286-03985-4", "978-4-286-03985-5" },
	{ "4-286-14518-2", "978-4-286-14518-1" },
	{ "4-286-14519-0", "978-4-286-14519-8" },
	{ "4-327-45278-5", "978-4-327-45278-0" },
	{ "4-334-75103-2", "978-4-334-75103-6" },
	{ "4-384-05366-5", "978-4-384-05366-1" },
	{ "4-393-45502-9", "978-4-393-45502-9" },
	{ "4-480-42160-2", "978-4-480-42160-9" },
	{ "4-569-70641-X", "978-4-569-70641-2" },
	{ "4-582-76562-9", "978-4-582-76562-5" },
	{ "4-582-83855-3", "978-4-582-83855-8" },
	{ "4-591-17039-X", "978-4-591-17039-7" },
	{ "4-591-09340-9", "978-4-591-09340-5" },
	{ "4-592-76121-9", "978-4-592-76121-1" },
	{ "4-622-07158-4", "978-4-622-07158-7" },
	{ "4-7662-0919-2", "978-4-7662-0919-8" },
	{ "4-7700-2795-8", "978-4-7700-2795-5" },
	{ "4-7744-0626-0", "978-4-7744-0626-8" },
	{ "4-7771-1105-9", "978-4-7771-1105-3" },
	{ "4-7946-0141-7", "978-4-7946-0141-4" },
	{ "4-7946-0147-6", "978-4-7946-0147-6" },
	{ "4-7946-0248-0", "978-4-7946-0248-0" },
	{ "4-7946-0540-4", "978-4-7946-0540-5" },
	{ "4-7946-0543-9", "978-4-7946-0543-6" },
	{ "4-7946-0589-7", "978-4-7946-0589-4" },
	{ "4-7966-4695-7", "978-4-7966-4695-6" },
	{ "4-7966-5307-4", "978-4-7966-5307-7" },
	{ "4-8019-0514-5", "978-4-8019-0514-6" },
	{ "4-8083-0879-7", "978-4-8083-0879-7" },
	{ "4-8086-0620-8", "978-4-8086-0620-6" },
	{ "4-8086-0643-7", "978-4-8086-0643-5" },
	{ "4-8454-5135-2", "978-4-8454-5135-7" },
	{ "4-8460-0443-0", "978-4-8460-0443-9" },
	{ "4-86164-077-6", "978-4-86164-077-3" },
	{ "4-86392-317-1", "978-4-86392-317-1" },
	{ "4-87097-102-X", "978-4-87097-102-8" },
	{ "4-87242-660-6", "978-4-87242-660-1" },
	{ "4-88595-737-0", "978-4-88595-737-6" },
	{ "4-89694-862-9", "978-4-89694-862-2" },
	{ "4-89684-574-9", "978-4-89684-574-7" },
	{ "4-89684-645-1", "978-4-89684-645-4" },
	{ "4-915076-35-0", "978-4-915076-35-0" },
	{ "5-519-65952-4", "978-5-519-65952-9" },
	{ "605-7861-54-X", "978-605-7861-54-2" },
	{ "617-660-307-2", "978-617-660-307-8" },
	{ "7-101-10476-2", "978-7-101-10476-9" },
	{ "7-119-03544-4", "978-7-119-03544-4" },
	{ "7-201-11624-X", "978-7-201-11624-2" },
	{ "7-5063-4197-2", "978-7-5063-4197-4" },
	{ "7-5063-4070-4", "978-7-5063-4070-0" },
	{ "7-5117-2465-5", "978-7-5117-2465-6" },
	{ "7-5190-0579-8", "978-7-5190-0579-5" },
	{ "7-5376-4674-0", "978-7-5376-4674-1" },
	{ "7-5387-3058-3", "978-7-5387-3058-6" },
	{ "7-5484-2508-2", "978-7-5484-2508-3" },
	{ "7-5669-0119-2", "978-7-5669-0119-4" },
	{ "7-5699-2186-0", "978-7-5699-2186-1" },
	{ "83-7887-634-9", "978-83-7887-634-2" },
	{ "84-321-4524-6", "978-84-321-4524-7" },
	{ "84-7888-629-X", "978-84-7888-629-6" },
	{ "85-349-4253-6", "978-85-349-4253-9" },
	{ "85-378-1466-0", "978-85-378-1466-6" },
	{ "85-380-6346-4", "978-85-380-6346-9" },
	{ "85-380-7484-9", "978-85-380-7484-7" },
	{ "85-89987-33-7", "978-85-89987-33-2" },
	{ "88-07-90169-2", "978-88-07-90169-0" },
	{ "88-11-81074-4", "978-88-11-81074-2" },
	{ "88-228-0034-6", "978-88-228-0034-3" },
	{ "88-264-3345-3", "978-88-264-3345-5" },
	{ "88-278-1845-6", "978-88-278-1845-9" },
	{ "88-301-0212-1", "978-88-301-0212-5" },
	{ "88-452-7778-X", "978-88-452-7778-8" },
	{ "88-541-7238-3", "978-88-541-7238-8" },
	{ "88-580-1283-6", "978-88-580-1283-3" },
	{ "88-6177-517-9", "978-88-6177-517-6" },
	{ "88-6188-935-2", "978-88-6188-935-4" },
	{ "88-6189-429-1", "978-88-6189-429-7" },
	{ "88-6559-195-1", "978-88-6559-195-6" },
	{ "88-6714-828-1", "978-88-6714-828-8" },
	{ "88-9309-606-4", "978-88-9309-606-5" },
	{ "88-98480-32-6", "978-88-98480-32-6" },
	{ "89-491-9013-3", "978-89-491-9013-6" },
	{ "89-546-0309-2", "978-89-546-0309-6" },
	{ "89-7358-479-0", "978-89-7358-479-6" },
	{ "89-957724-2-5", "978-89-957724-2-3" },
	{ "89-98046-66-0", "978-89-98046-66-8" },
	{ "93-86538-22-9", "978-93-86538-22-2" },
	{ "958-8300-97-5", "978-958-8300-97-9" },
	{ "972-20-5593-3", "978-972-20-5593-2" },
	{ "986-318-077-7", "978-986-318-077-7" },
	{ "986-95844-5-4", "978-986-95844-5-6" },
	{ "987-751-430-3", "978-987-751-430-8" },
	{ NULL,            "979-8-6210-8135-5" },
};

ATF_TC(test_isbn10_strip);
ATF_TC_HEAD(test_isbn10_strip, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_isbn10_strip");
}
ATF_TC_BODY(test_isbn10_strip, tc)
{
	size_t i;
	const struct testcase *p;
	char isbn10[HYPHEN10_LEN + 1], buf[ISBN10_LEN + 1], *t;
	const char *s;
	int c;

	for (i = 0; i < arraycount(testcases); ++i) {
		p = &testcases[i];
		if (p->isbn10 == NULL)
			continue;
		s = p->isbn10;
		t = isbn10;
		while ((c = *s++) != '\0') {
			if (c == '-')
				continue;
			*t++ = c;
		}
		*t = '\0';
		/* normal case */
		ATF_CHECK(!isbn10_strip(p->isbn10, buf, sizeof(buf)));
		ATF_CHECK(!strcmp(isbn10, buf));
		/* normal case(no check digit) */
		isbn10[ISBN10_LEN - 1] = '\0';
		ATF_CHECK(!isbn10_strip(p->isbn10, buf, sizeof(buf) - 1));
		ATF_CHECK(!strcmp(isbn10, buf));
		/* error(insufficient buffer) */
		ATF_CHECK(isbn10_strip(p->isbn10, buf, sizeof(buf) - 2));
		/* error(incomplete isbn10) */
		memcpy(isbn10, p->isbn10, HYPHEN10_LEN - 3);
		isbn10[HYPHEN10_LEN - 3] = '\0';
		ATF_CHECK(isbn10_strip(isbn10, buf, sizeof(buf)));
	}
}

ATF_TC(test_isbn13_strip);
ATF_TC_HEAD(test_isbn13_strip, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_isbn13_strip");
}
ATF_TC_BODY(test_isbn13_strip, tc)
{
	size_t i;
	const struct testcase *p;
	char isbn13[HYPHEN13_LEN + 1], buf[ISBN13_LEN + 1], *t;
	const char *s;
	int c;

	for (i = 0; i < arraycount(testcases); ++i) {
		p = &testcases[i];
		s = p->isbn13;
		t = isbn13;
		while ((c = *s++) != '\0') {
			if (c == '-')
				continue;
			*t++ = c;
		}
		*t = '\0';
		/* normal case */
		ATF_CHECK(!isbn13_strip(p->isbn13, buf, sizeof(buf)));
		ATF_CHECK(!strcmp(isbn13, buf));
		/* normal case(no check digit) */
		isbn13[ISBN13_LEN - 1] = '\0';
		ATF_CHECK(!isbn13_strip(p->isbn13, buf, sizeof(buf) - 1));
		ATF_CHECK(!strcmp(isbn13, buf));
		/* error(insufficient buffer) */
		ATF_CHECK(isbn13_strip(p->isbn13, buf, sizeof(buf) - 2));
		/* error(incomplete isbn13) */
		memcpy(isbn13, p->isbn13, HYPHEN13_LEN - 3);
		isbn13[HYPHEN13_LEN - 3] = '\0';
		ATF_CHECK(isbn13_strip(isbn13, buf, sizeof(buf)));
	}
}

ATF_TC(test_isbn10_checkdigit);
ATF_TC_HEAD(test_isbn10_checkdigit, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_isbn10_checkdigit");
}
ATF_TC_BODY(test_isbn10_checkdigit, tc)
{
	size_t i;
	const struct testcase *p;
	int cd0, cd1;
	char buf[HYPHEN10_LEN + 1];

	for (i = 0; i < arraycount(testcases); ++i) {
		p = &testcases[i];
		if (p->isbn10 == NULL)
			continue;
		/* normal case */
		cd0 = p->isbn10[HYPHEN10_LEN - 1];
		ATF_CHECK(!isbn10_checkdigit(p->isbn10, &cd1));
		ATF_CHECK(cd0 == cd1);
		/* normal case(no check digit) */
		memcpy(buf, p->isbn10, HYPHEN10_LEN - 1);
		buf[HYPHEN10_LEN - 1] = '\0';
		ATF_CHECK(!isbn10_checkdigit(buf, &cd1));
		ATF_CHECK(cd0 == cd1);
		/* error(incomplete isbn10) */
		buf[HYPHEN10_LEN - 3] = '\0';
		ATF_CHECK(isbn10_checkdigit(buf, &cd1));
		/* normal case(stripped) */
		ATF_CHECK(!isbn10_strip(p->isbn10, buf, sizeof(buf)));
		ATF_CHECK(!isbn10_checkdigit(buf, &cd1));
		ATF_CHECK(cd0 == cd1);
		/* normal case(stripped + no check digit) */
		buf[ISBN10_LEN - 1] = '\0';
		ATF_CHECK(!isbn10_checkdigit(buf, &cd1));
		ATF_CHECK(cd0 == cd1);
		/* error(incomplete stripped isbn10) */
		buf[ISBN10_LEN - 2] = '\0';
		ATF_CHECK(isbn10_checkdigit(buf, &cd1));
	}
}

ATF_TC(test_isbn13_checkdigit);
ATF_TC_HEAD(test_isbn13_checkdigit, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_isbn13_checkdigit");
}
ATF_TC_BODY(test_isbn13_checkdigit, tc)
{
	size_t i;
	const struct testcase *p;
	int cd0, cd1;
	char buf[HYPHEN13_LEN + 1];

	for (i = 0; i < arraycount(testcases); ++i) {
		p = &testcases[i];
		/* normal case */
		cd0 = p->isbn13[HYPHEN13_LEN - 1];
		ATF_CHECK(!isbn13_checkdigit(p->isbn13, &cd1));
		ATF_CHECK(cd0 == cd1);
		/* normal case(no check digit) */
		memcpy(buf, p->isbn13, HYPHEN13_LEN - 1);
		buf[HYPHEN13_LEN - 1] = '\0';
		ATF_CHECK(!isbn13_checkdigit(buf, &cd1));
		ATF_CHECK(cd0 == cd1);
		/* error(incomplete isbn13) */
		buf[HYPHEN13_LEN - 3] = '\0';
		ATF_CHECK(isbn13_checkdigit(buf, &cd1));
		/* normal case(stripped) */
		ATF_CHECK(!isbn13_strip(p->isbn13, buf, sizeof(buf)));
		ATF_CHECK(!isbn13_checkdigit(buf, &cd1));
		ATF_CHECK(cd0 == cd1);
		/* normal case(stripped + no check digit) */
		buf[ISBN13_LEN - 1] = '\0';
		ATF_CHECK(!isbn13_checkdigit(buf, &cd1));
		ATF_CHECK(cd0 == cd1);
		/* error(incomplete stripped isbn13) */
		buf[ISBN13_LEN - 2] = '\0';
		ATF_CHECK(isbn13_checkdigit(buf, &cd1));
	}
}

ATF_TC(test_isbn10_validate);
ATF_TC_HEAD(test_isbn10_validate, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_isbn10_validate");
}
ATF_TC_BODY(test_isbn10_validate, tc)
{
	static const char *isbn10cd = "0123456789X0";
	size_t i;
	const struct testcase *p;
	int bad;
	char buf[HYPHEN10_LEN + 1];

	for (i = 0; i < arraycount(testcases); ++i) {
		p = &testcases[i];
		if (p->isbn10 == NULL)
			continue;
		/* normal case */
		ATF_CHECK(isbn10_validate(p->isbn10) == true);
		/* error(bad check digit) */
		bad = strchr(isbn10cd, p->isbn10[HYPHEN10_LEN - 1])[1];
		memcpy(buf, p->isbn10, HYPHEN10_LEN - 1);
		buf[HYPHEN10_LEN - 1] = bad;
		ATF_CHECK(isbn10_validate(buf) == false);
		/* error(no check digit) */
		buf[HYPHEN10_LEN - 1] = '\0';
		ATF_CHECK(isbn10_validate(buf) == false);
		/* error(incomplete isbn10) */
		buf[HYPHEN10_LEN - 3] = '\0';
		ATF_CHECK(isbn10_validate(buf) == false);
		/* normal case(stripped) */
		ATF_CHECK(!isbn10_strip(p->isbn10, buf, sizeof(buf)));
		ATF_CHECK(isbn10_validate(buf) == true);
		/* error(stripped + bad check digit) */
		buf[ISBN10_LEN - 1] = bad;
		ATF_CHECK(isbn10_validate(buf) == false);
		/* error(stripped + no check digit) */
		buf[ISBN10_LEN - 1] = '\0';
		ATF_CHECK(isbn10_validate(buf) == false);
		/* error(stripped + incomplete isbn10) */
		buf[ISBN10_LEN - 2] = '\0';
		ATF_CHECK(isbn10_validate(buf) == false);
	}
}

ATF_TC(test_isbn13_validate);
ATF_TC_HEAD(test_isbn13_validate, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_isbn13_validate");
}
ATF_TC_BODY(test_isbn13_validate, tc)
{
	static const char *isbn13cd = "01234567890";
	size_t i;
	const struct testcase *p;
	int bad;
	char buf[HYPHEN13_LEN + 1];

	for (i = 0; i < arraycount(testcases); ++i) {
		p = &testcases[i];
		/* normal case */
		ATF_CHECK(isbn13_validate(p->isbn13) == true);
		/* error(bad check digit) */
		bad = strchr(isbn13cd, p->isbn13[HYPHEN13_LEN - 1])[1];
		memcpy(buf, p->isbn13, HYPHEN13_LEN - 1);
		buf[HYPHEN13_LEN - 1] = bad;
		ATF_CHECK(isbn13_validate(buf) == false);
		/* error(no check digit) */
		buf[HYPHEN13_LEN - 1] = '\0';
		ATF_CHECK(isbn13_validate(buf) == false);
		/* error(incomplete isbn13) */
		buf[HYPHEN13_LEN - 3] = '\0';
		ATF_CHECK(isbn13_validate(buf) == false);
		/* normal case(stripped) */
		ATF_CHECK(!isbn13_strip(p->isbn13, buf, sizeof(buf)));
		ATF_CHECK(isbn13_validate(buf) == true);
		/* error(stripped + bad check digit) */
		buf[ISBN13_LEN - 1] = bad;
		ATF_CHECK(isbn13_validate(buf) == false);
		/* error(stripped + no check digit) */
		buf[ISBN13_LEN - 1] = '\0';
		ATF_CHECK(isbn13_validate(buf) == false);
		/* error(stripped + incomplete isbn13) */
		buf[ISBN13_LEN - 2] = '\0';
		ATF_CHECK(isbn13_validate(buf) == false);
	}
}

ATF_TC(test_isbn10_to_isbn13);
ATF_TC_HEAD(test_isbn10_to_isbn13, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_isbn10_to_isbn13");
}
ATF_TC_BODY(test_isbn10_to_isbn13, tc)
{
	size_t i;
	const struct testcase *p;
	char strip10[ISBN10_LEN + 1], strip13[ISBN13_LEN + 1], buf[ISBN13_LEN + 1];

	for (i = 0; i < arraycount(testcases); ++i) {
		p = &testcases[i];
		if (p->isbn10 == NULL)
			continue;
		/* normal case */
		ATF_CHECK(!isbn10_strip(p->isbn10, strip10, sizeof(strip10)));
		ATF_CHECK(!isbn13_strip(p->isbn13, strip13, sizeof(strip13)));
		ATF_CHECK(!isbn10_to_isbn13(p->isbn10, buf, sizeof(buf)));
		ATF_CHECK(!strcmp(strip13, buf));
		/* normal case(stripped) */
		ATF_CHECK(!isbn10_to_isbn13(strip10, buf, sizeof(buf)));
		ATF_CHECK(!strcmp(strip13, buf));
	}
}

ATF_TC(test_isbn13_to_isbn10);
ATF_TC_HEAD(test_isbn13_to_isbn10, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_isbn13_to_isbn10");
}
ATF_TC_BODY(test_isbn13_to_isbn10, tc)
{
	size_t i;
	const struct testcase *p;
	char strip13[ISBN13_LEN + 1], strip10[ISBN10_LEN + 1], buf[ISBN10_LEN + 1];

	for (i = 0; i < arraycount(testcases); ++i) {
		p = &testcases[i];
		ATF_CHECK(!isbn13_strip(p->isbn13, strip13, sizeof(strip13)));
		/* error(isbn13 isn't start with 978) */
		if (p->isbn10 == NULL) {
			ATF_CHECK(isbn13_to_isbn10(p->isbn13,
			    buf, sizeof(buf)));
			ATF_CHECK(isbn13_to_isbn10(strip13,
			    buf, sizeof(buf)));
			continue;
		}
		/* normal case */
		ATF_CHECK(!isbn10_strip(p->isbn10, strip10, sizeof(strip10)));
		ATF_CHECK(!isbn13_to_isbn10(p->isbn13, buf, sizeof(buf)));
		ATF_CHECK(!strcmp(strip10, buf));
		/* normal case(stripped) */
		ATF_CHECK(!isbn13_to_isbn10(strip13, buf, sizeof(buf)));
		ATF_CHECK(!strcmp(strip10, buf));
	}
}

ATF_TC(test_isbn10_hyphenate);
ATF_TC_HEAD(test_isbn10_hyphenate, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_isbn10_hyphenate");
}
ATF_TC_BODY(test_isbn10_hyphenate, tc)
{
	size_t i;
	const struct testcase *p;
	char strip10[ISBN10_LEN + 1], buf[HYPHEN10_LEN + 1];

	for (i = 0; i < arraycount(testcases); ++i) {
		p = &testcases[i];
		if (p->isbn10 == NULL)
			continue;
		/* normal case */
		ATF_CHECK(!isbn10_strip(p->isbn10, strip10, sizeof(strip10)));
		ATF_CHECK(!isbn10_hyphenate(strip10, buf, sizeof(buf)));
		ATF_CHECK(!strcmp(p->isbn10, buf));
		/* error(insufficient buffer) */
		ATF_CHECK(isbn10_hyphenate(strip10, buf, sizeof(buf) - 1));
		/* error(incomplete isbn10) */
		strip10[ISBN10_LEN - 1] = '\0';
		ATF_CHECK(isbn10_hyphenate(strip10, buf, sizeof(buf)));
		/* error(not stripped) */
		ATF_CHECK(isbn10_hyphenate(p->isbn10, buf, sizeof(buf)));
	}
}

ATF_TC(test_isbn13_hyphenate);
ATF_TC_HEAD(test_isbn13_hyphenate, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_isbn13_hyphenate");
}
ATF_TC_BODY(test_isbn13_hyphenate, tc)
{
	size_t i;
	const struct testcase *p;
	char strip13[ISBN13_LEN + 1], buf[HYPHEN13_LEN + 1];

	for (i = 0; i < arraycount(testcases); ++i) {
		p = &testcases[i];
		/* normal case */
		ATF_CHECK(!isbn13_strip(p->isbn13, strip13, sizeof(strip13)));
		ATF_CHECK(!isbn13_hyphenate(strip13, buf, sizeof(buf)));
		ATF_CHECK(!strcmp(p->isbn13, buf));
		/* error(insufficient buffer) */
		ATF_CHECK(isbn13_hyphenate(strip13, buf, sizeof(buf) - 1));
		/* error(incomplete isbn13) */
		strip13[ISBN13_LEN - 1] = '\0';
		ATF_CHECK(isbn13_hyphenate(strip13, buf, sizeof(buf)));
		/* error(not stripped) */
		ATF_CHECK(isbn13_hyphenate(p->isbn13, buf, sizeof(buf)));
	}
}

ATF_TP_ADD_TCS(tp)
{
	ATF_TP_ADD_TC(tp, test_isbn10_strip);
	ATF_TP_ADD_TC(tp, test_isbn13_strip);
	ATF_TP_ADD_TC(tp, test_isbn10_checkdigit);
	ATF_TP_ADD_TC(tp, test_isbn13_checkdigit);
	ATF_TP_ADD_TC(tp, test_isbn10_validate);
	ATF_TP_ADD_TC(tp, test_isbn13_validate);
	ATF_TP_ADD_TC(tp, test_isbn10_to_isbn13);
	ATF_TP_ADD_TC(tp, test_isbn13_to_isbn10);
	ATF_TP_ADD_TC(tp, test_isbn10_hyphenate);
	ATF_TP_ADD_TC(tp, test_isbn13_hyphenate);

	return atf_no_error();
}
