# libISBN - ISBN(International Standard Book Number) string manipulation operations

- calculate ISBN10/13 check digit
- validate ISBN10/13
- convert ISBN10 <-> ISBN13
- hyphenate ISBN10/13
- strip ISBN10/13

## Build prequisite
- ISO/IEC 9899:1999 environment
- [libtool](https://www.gnu.org/software/libtool/) >=2.4.6
- [autoconf](https://www.gnu.org/software/autoconf/autoconf.html) >=2.71
- [automake](https://www.gnu.org/software/automake/) >=2.16.3

## How to install
```sh
$ git clone -b master https://tnozaki@bitbucket.org/tnozaki/isbn.git
$ cd isbn
$ ./autogen.sh
$ ./configure
$ make && make check && sudo make install
$ make distclean
```

## License
2-clause BSDL
```text
Copyright (c) 2021 Takehiko NOZAKI,
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
```
