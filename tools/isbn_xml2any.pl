#!/usr/pkg/bin/perl

=head1 COPYRIGHT & LICENSE

 Copyright (c) 2021 Takehiko NOZAKI,
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

=head1 NAME

 isbn_xml2any.pl - parse ISBN RangeMessage.xml and generate code

=head1 SYNOPSIS

 isbn_xml2any.pl [options]

=head1 OPTIONS

=over 8

=item B<--format>

Specify output format, json(default) or c.

=item B<--help>

Print this help message.

=back
 
=cut

use strict;
use warnings;
use File::Spec;
use File::Temp qw(mkstemp);
use Getopt::Long;
use JSON;
use LWP::UserAgent;
use Pod::Simple::Text;
use Pod::Usage;
use XML::XPath;
use XML::XPath::XMLParser;
use Template;

my $base_url = 'https://www.isbn-international.org';
my $info_url = "${base_url}/bl_proxy/GetRangeInformations";
my $download_url = "${base_url}/download_range/%s/%s";

my $meta_name = {
    'Source' => '/ISBNRangeMessage/MessageSource/text()',
    'SerialNumber' => '/ISBNRangeMessage/MessageSerialNumber/text()',
    'Date' => '/ISBNRangeMessage/MessageDate/text()'
};
my $lengths_map_name = {
    'groups_length'    => '/ISBNRangeMessage/EAN.UCCPrefixes/EAN.UCC',
    'publisher_length' => '/ISBNRangeMessage/RegistrationGroups/Group'
};

sub parse_copyright($$)
{
    my ($filename, $copyright) = @_;

    my $ps = new Pod::Simple::Text;
    my $out;
    $ps->output_string(\$out);
    $ps->parse_file($filename);
    my @lines = split(/\n/, $out);
    my $in_copyright = 0;
    foreach my $line (@lines) {
        if ($line =~ m/^COPYRIGHT/) {
            $in_copyright = 1;
        } elsif ($line =~ m/^[[:upper:]]+/) {
            $in_copyright = 0;
        } elsif ($in_copyright) {
            $line =~ s/^[[:space:]]+//;
            push(@{$copyright}, $line);
        }
    }
    shift(@{$copyright});
    pop(@{$copyright});
}

sub parse_lengths_map($$$$)
{
    my ($xpath, $name, $path, $isbn) = @_;

    foreach my $lengths_map ($xpath->find($path)->get_nodelist) {
        my $prefix = $xpath->find('Prefix/text()',
	  $lengths_map)->string_value;
        foreach my $rule ($xpath->find('Rules/Rule',
	  $lengths_map)->get_nodelist) {
            my $range = $xpath->find('Range/text()', $rule)->string_value;
            die unless ($range =~ m/^([[:digit:]]+)-([[:digit:]]+)$/);
            my ($min, $max) = ($1, $2);
            my $length = $xpath->find('Length/text()', $rule)->string_value;
            die unless ($length =~ m/^([[:digit:]])$/);
            push(@{$isbn->{lengths_map}->{$name}->{$prefix}}, {
                min => int($min),
                max => int($max),
                length => int($length)
            });
        }
    }
}

my $format = 'json';
my $help = 0;
GetOptions(
    "format=s" => \$format,
    "help|?" => \$help
) || die;
pod2usage(1) if $help || $#ARGV > 0;

my $copyright = [];
parse_copyright($0, $copyright);
my $ua = new LWP::UserAgent;
my %form = (
  format => 1,
  language => 'en',
  translatedTexts => 'Printed;Last+Change'
);
my $res = $ua->post($info_url, \%form);
die $res->status_line unless ($res->is_success);
my $json_result = JSON->new->utf8->decode($res->decoded_content);
die unless ($json_result->{'status'} eq 'success');
my $url = sprintf($download_url,
  $json_result->{'result'}->{'value'},
  $json_result->{'result'}->{'filename'}
);
$res = $ua->get($url);
die $res->status_line unless ($res->is_success);
my $tmpdir = File::Spec->tmpdir();
my ($fh, $file) = mkstemp("${tmpdir}/isbn_xml2anyXXXXX");
unlink($file);
binmode($fh, ':encoding(utf-8)');
print $fh $res->decoded_content;
seek($fh, 0, 0);
my $xpath = XML::XPath->new(ioref => $fh);
my $isbn = {};
while (my ($name, $path) = each(%{$meta_name})) {
    $isbn->{meta}->{$name} = $xpath->find($path)->string_value;
}
while (my ($name, $path) = each(%{$lengths_map_name})) {
    parse_lengths_map($xpath, $name, $path, $isbn);
}
close($fh);
my $out;
if ($format eq 'c') {
    my $tt = new Template();
    $tt->process(\*DATA, {
	copyright => $copyright,
	isbn => $isbn
    }, \$out) || die $tt->error;
} elsif ($format eq 'json') {
    $out = JSON->new->utf8->canonical->pretty->encode($isbn);
} else {
    die;
}
print $out, "\n";
__DATA__
[% USE format -%]
[% rfmt = format('%7d') -%]
/*-
[% FOREACH line IN copyright -%]
 * [% line %]
[% END -%]
 */

#ifndef ISBN_TAB_H_
#define ISBN_TAB_H_

/*
 * THIS FILE AUTOMATICALLY GENERATED.  DO NOT EDIT.
 *
 * generated from:
 * RangeMessage.xml(https://www.isbn-international.org/range_file_generation)
[% FOREACH name IN isbn.meta.keys.sort -%]
 * [[% name %]: [% isbn.meta.$name %]]
[% END -%]
 */

struct range {
	unsigned int min, max;
	size_t length;
};

struct lengths_map {
	const char *prefix;
	const struct range *ranges;
};

[% FOREACH name IN isbn.lengths_map.keys.sort -%]
[% lengths = isbn.lengths_map.$name -%]
[% FOREACH prefix IN lengths.keys.sort -%]
[% ranges = lengths.$prefix -%]
static const struct range [% name %]_[% prefix.replace('-', '_') %][] = {
[% FOREACH range IN ranges -%]
	{ [%- rfmt(range.min) -%], [%- rfmt(range.max) -%],  [%- range.length -%] },
[% END -%]
	{ [% rfmt(0) %], [% rfmt(0) %], -1 }
};

[% END -%]
[% END -%]
[% FOREACH name IN isbn.lengths_map.keys.sort -%]
[% lengths = isbn.lengths_map.$name -%]
static const struct lengths_map [% name %][] = {
[% FOREACH prefix IN lengths.keys.sort -%]
	{
		"[% prefix %]",
		[% name %]_[% prefix.replace('-', '_') %]
	},
[% END -%]
	{
		NULL,
		NULL
	}
};

[% END -%]
#endif /* ISBN_TAB_H_ */
