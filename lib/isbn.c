/*-
 * Copyright (c) 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "isbn_local.h"
#include "isbn_tab.h"

static inline int
isbn_isdigit(int c)
{
	return c >= '0' && c <= '9';
}

static size_t
strip(const char * restrict * restrict isbn, char * restrict s, size_t n)
{
	const char *src;
	char *dst;

	assert(isbn != NULL && *isbn != NULL);
	assert(s != NULL);

	src = *isbn;
	dst = s;
	while (*src != '\0') {
		if (isbn_isdigit((unsigned char)*src))
			break;
		++src;
	}
	while (*src != '\0') {
		if (isbn_isdigit((unsigned char)*src)) {
			if (n-- < 1)
				break;
			*dst++ = *src;
		} else if (*src != '-')
			break;
		++src;
	}
	*isbn = src;
	return (size_t)(dst - s);
}

static inline int
isbn10_checkdigit_unsafe(const char *isbn10)
{
	static const char isbn10digits[12] = "0123456789X0";
	int acc, i, c;

	assert(isbn10 != NULL);

	acc = 0;
	for (i = 0; i < ISBN10_LEN - 1; ++i) {
		c = isbn10[i];
		acc += (c - '0') * (10 - i);
	}
	return isbn10digits[11 - acc % 11];
}

int
isbn10_checkdigit(const char * restrict isbn10, int * restrict checkdigit)
{
	char buf[ISBN10_LEN - 1];

	assert(isbn10 != NULL);
	assert(checkdigit != NULL);

	if (strip(&isbn10, buf, sizeof(buf)) != sizeof(buf))
		return EINVAL;
	*checkdigit = isbn10_checkdigit_unsafe(buf);
	return 0;
}

static inline int
isbn13_checkdigit_unsafe(const char *isbn13)
{
	static const char isbn13digits[11] = "01234567890";
	static const int isbn13multi[2] = { 1, 3 };
	int acc, i, c;

	assert(isbn13 != NULL);

	acc = 0;
	for (i = 0; i < ISBN13_LEN - 1; ++i) {
		c = isbn13[i];
		acc += (c - '0') * isbn13multi[i % 2];
	}
	return isbn13digits[10 - acc % 10];
}

int
isbn13_checkdigit(const char * restrict isbn13, int * restrict checkdigit)
{
	char buf[ISBN13_LEN - 1];

	assert(isbn13 != NULL);
	assert(checkdigit != NULL);

	if (strip(&isbn13, buf, sizeof(buf)) != sizeof(buf))
		return EINVAL;
	*checkdigit = isbn13_checkdigit_unsafe(buf);
	return 0;
}

bool
isbn10_validate(const char *isbn10)
{
	char buf[ISBN10_LEN - 1];

	assert(isbn10 != NULL);

	if (strip(&isbn10, buf, sizeof(buf)) != sizeof(buf))
		return false;
	return *isbn10 == isbn10_checkdigit_unsafe(buf);
}

bool
isbn13_validate(const char *isbn13)
{
	char buf[ISBN13_LEN - 1];

	assert(isbn13 != NULL);

	if (strip(&isbn13, buf, sizeof(buf)) != sizeof(buf))
		return false;
	return *isbn13 == isbn13_checkdigit_unsafe(buf);
}

int
isbn10_to_isbn13(const char * restrict isbn10, char * restrict s, size_t n)
{
	char *t;

	assert(isbn10 != NULL);
	assert(s != NULL);

	if (n <= ISBN13_LEN)
		return E2BIG;
	t = s;
	memcpy(t, DEFAULT_GS1, GS1_LEN);
	t += GS1_LEN;
	if (strip(&isbn10, t, ISBN10_LEN - 1) != ISBN10_LEN - 1)
		return EINVAL;
	t += ISBN10_LEN - 1;
	*t++ = isbn13_checkdigit_unsafe(s);
	*t = '\0';
	return 0;
}

int
isbn13_to_isbn10(const char * restrict isbn13, char * restrict s, size_t n)
{
	char buf[ISBN13_LEN - 1], *t;

	assert(isbn13 != NULL);
	assert(s != NULL);

	if (n <= ISBN10_LEN)
		return E2BIG;
	if (strip(&isbn13, buf, sizeof(buf)) != sizeof(buf) ||
	    memcmp(buf, DEFAULT_GS1, GS1_LEN))
		return EINVAL;
	t = s;
	memcpy(t, &buf[GS1_LEN], ISBN10_LEN - 1);
	t += ISBN10_LEN - 1;
	*t++ = isbn10_checkdigit_unsafe(s);
	*t = '\0';
	return 0;
}

static inline int
prefixlen(const struct lengths_map *lm,
    const char *prefix, unsigned int first7, size_t *ret)
{
	const struct range *r;

	assert(lm != NULL);
	assert(prefix != NULL);
	assert(ret != NULL);

	for (; lm->prefix != NULL; ++lm) {
		if (strcmp(prefix, lm->prefix))
			continue;
		for (r = lm->ranges; r->length != (size_t)-1; ++r) {
			if (first7 < r->min || first7 > r->max)
				continue;
			*ret = r->length;
			return 0;
		}
	}
	return EINVAL;
}

static inline unsigned int
first7(const char *s)
{
	static const char zeros[7] = "0000000";
	char buf[sizeof(zeros) + 1], *p;
	size_t len;
	unsigned long l;

	assert(s != NULL);

	len = strlen(s);
	memcpy(buf, s, len);
	if (len < sizeof(zeros))
		memcpy(&buf[len], zeros, sizeof(zeros) - len);
	buf[sizeof(zeros)] = '\0';
	l = strtoul(buf, &p, 10);
	assert(p == &buf[sizeof(zeros)]);
	assert(l <= 9999999);
	return (unsigned int)l;
}

int
isbn10_hyphenate(const char * restrict src, char * restrict dst, size_t dlen)
{
	size_t slen;
	char isbn13[ISBN13_LEN + 1], hyphen13[HYPHEN13_LEN + 1], *t;
	int ret;

	slen = strlen(src);
	if (slen != ISBN10_LEN)
		return EINVAL;
	if (dlen <= HYPHEN10_LEN)
		return E2BIG;
	t = isbn13;
	memcpy(t, DEFAULT_GS1, GS1_LEN);
	t += GS1_LEN;
	memcpy(t, src, ISBN10_LEN - 1);
	t += ISBN10_LEN - 1, src += ISBN10_LEN - 1;
	*t++ = isbn13_checkdigit_unsafe(isbn13);
	*t = '\0';
	ret = isbn13_hyphenate(isbn13, hyphen13, sizeof(hyphen13));
	if (ret)
		return ret;
	memcpy(dst, &hyphen13[GS1_LEN + 1], HYPHEN10_LEN - 1);
	dst += HYPHEN10_LEN - 1;
	*dst++ = *src;
	*dst = '\0';
	return 0;
}

int
isbn13_hyphenate(const char * restrict src, char * restrict dst, size_t dlen)
{
	static const struct lengths_map * const lm[] = {
		groups_length, publisher_length
	};
	size_t slen, n, i;
	const char *prefix;
	int ret;

	assert(src != NULL);
	assert(dst != NULL);

	slen = strlen(src);
	if (slen != ISBN13_LEN)
		return EINVAL;
	if (dlen <= HYPHEN13_LEN)
		return E2BIG;
	prefix = dst;
	n = GS1_LEN;
	memcpy(dst, src, n);
	dst += n, dlen -= n, src += n, slen -= n;
	*dst = '\0';
	for (i = 0; i < arraycount(lm); ++i) {
		ret = prefixlen(lm[i], prefix, first7(src), &n);
		if (ret)
			return ret;
		assert(dlen > n);
		assert(slen >= n);
		*dst++ = '-';
		memcpy(dst, src, n);
		dst += n, dlen -= n + 1, src += n, slen -= n;
		*dst = '\0';
	}
	assert(slen >= 2);
	assert(dlen - slen >= 3);
	*dst++ = '-';
	memcpy(dst, src, --slen);
	dst += slen, src += slen;
	*dst++ = '-';
	*dst++ = *src;
	*dst = '\0';
	return 0;
}

int
isbn10_strip(const char * restrict isbn10, char * restrict s, size_t n)
{
	size_t len;
	int c;

	if (n < ISBN10_LEN)
		return E2BIG;
	len = ISBN10_LEN - 1;
	if (strip(&isbn10, s, len) != len)
		return EINVAL;
	s += len, n -= len;
	if (n > 1) {
		c = (unsigned char)*isbn10;
		if (c != '\0') {
			if (!isbn_isdigit(c) && c != 'X')
				return EINVAL;
			*s++ = c;
		}
	}
	*s = '\0';
	return 0;
}

int
isbn13_strip(const char * restrict isbn13, char * restrict s, size_t n)
{
	size_t len;
	int c;

	if (n < ISBN13_LEN)
		return E2BIG;
	len = ISBN13_LEN - 1;
	if (strip(&isbn13, s, len) != len)
		return EINVAL;
	s += len, n -= len;
	if (n > 1) {
		c = (unsigned char)*isbn13;
		if (c != '\0') {
			if (!isbn_isdigit(c))
				return EINVAL;
			*s++ = c;
		}
	}
	*s = '\0';
	return 0;
}
